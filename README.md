[![Build Status](https://travis-ci.org/Heather/Control.Eternal.png?branch=master)](https://travis-ci.org/Heather/Control.Eternal) <br/>
![CodeShip](https://codeship.com/projects/a8451bf0-06c1-0133-4176-2aa9a23a545f/status?branch=master)

 - `<|` `|>` `<<|` `|>>` high priority pipes
 - Unicode operators
 - some flipped operators
 - LiftM_ LiftM2_ LiftM3_ LiftM4_ - non pure lift
 - Async Reactive

Hackage / cabal-install
=======================

```shell
cabal install eternal
```

Build with stack
================

``` shell
cd Control.Eternal
stack install
```

![](http://i.imgur.com/WQNpcjf.jpg)
