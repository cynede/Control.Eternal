{-# LANGUAGE Safe #-}

module Control.Eternal
  ( module Eternal
  ) where

import Control.Eternal.Syntax as Eternal
import Control.Eternal.Reactive as Eternal
