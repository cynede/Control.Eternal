{-# LANGUAGE
    UnicodeSyntax
  , Safe
  #-}

module Control.Eternal.Syntax.Lift
  ( liftM_
  , liftM2_
  , liftM3_
  , liftM4_
  ) where

import Control.Monad (join, liftM, liftM2, liftM3, liftM4)

liftM_  ∷ Monad m ⇒ (α1 → m α) → m α1 → m α
liftM2_ ∷ Monad m ⇒ (α1 → α2 → m α) → m α1 → m α2 → m α
liftM3_ ∷ Monad m ⇒ (α1 → α2 → α3 → m α) → m α1 → m α2 → m α3 → m α
liftM4_ ∷ Monad m ⇒ (α1 → α2 → α3 → α4 → m α) → m α1 → m α2 → m α3 → m α4 → m α

liftM_ a1 r           = join $ liftM a1 r
liftM2_ a1 a2 r       = join $ liftM2 a1 a2 r
liftM3_ a1 a2 a3 r    = join $ liftM3 a1 a2 a3 r
liftM4_ a1 a2 a3 a4 r = join $ liftM4 a1 a2 a3 a4 r
