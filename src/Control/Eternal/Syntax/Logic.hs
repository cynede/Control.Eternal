{-# LANGUAGE
    UnicodeSyntax
  , Safe
  #-}

module Control.Eternal.Syntax.Logic
  ( ifSo
  , ifNot
  ) where

import Control.Monad (when, unless)

ifSo ∷ IO () → Bool → IO ()
ifSo = flip when

ifNot ∷ IO () → Bool → IO ()
ifNot = flip unless
