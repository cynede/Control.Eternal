{-# LANGUAGE
    UnicodeSyntax
  , Safe
  #-}

module Control.Eternal.Syntax.Operators
  ( (<|)
  , (|>)
  , (<<|)
  , (|>>)
  ) where

infixl 2 <|, |>

(<|) :: (α → β) → α → β
f <| a = f a

(|>) :: α → (α → β) → β
a |> f = f a

infixl 7 <<|, |>>

(<<|) :: (α → β) → α → β
f <<| a = f a

(|>>) :: α → (α → β) → β
a |>> f = f a
