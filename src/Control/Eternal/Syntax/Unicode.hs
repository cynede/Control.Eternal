{-# LANGUAGE
    UnicodeSyntax
  , Safe
  #-}

module Control.Eternal.Syntax.Unicode
  ( (⊳)
  , (⊲)
  ) where

infixl 2 ⊳, ⊲

(⊳) :: α → (α → β) → β
a ⊳ f = f a

(⊲) :: (α → β) → α → β
f ⊲ a = f a
